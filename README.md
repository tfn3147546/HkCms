# HkCms

#### 介绍
HkCms 开源内容管理系统《免授权，永久免费商用》是一款基于 ThinkPHP6.0 开发的 CMS 系统。以免费开源、无需授权、系统易安装升级、界面功能简洁轻便、易上手、插件与模板在线升级安装、建站联盟扶持计划等优势为一体的 CMS 系统。

#### HkCms 免费开源，免授权，永久商用
近期有很多网友咨询我们，HkCms 是否会收取授权费用，我们特此声明，HkCms 免费开源，无需授权，永久免费商用，在您遵守<a href="https://www.hkcms.cn/instructions/24.html" target="_blank">《HkCms 软件许可使用协议》</a>下可将 HkCms 开源内容管理系统用于商业。

从 ShuipFCms 到 LvYeCms，再到 HkCms，我们从未停下为建站行业提供优秀的免费开源内容管理系统的初心，我们希望将免费开源进行到底。

各位网友务必在遵守国家法律法规的前提下使用 HkCms 开源内容管理系统，禁止使用 HkCms 开源内容管理系统进行任何违法犯罪的活动，使用 HkCms 开源内容管理系统过程中产生的任何版权纠纷及法律责任由使用方承担。

一个优秀的开源内容管理系统离不开网友的建议、帮助和支持，我们希望与广大网友一起完善我们的开源内容管理系统，在使用过程中有任何的建议和思路都可以通过 QQ 群：<a href="https://jq.qq.com/?_wv=1027&k=8YzV4elJ" target="_blank">808251031</a> 向我们反馈。

#### 软件特点
- 免费开源、无需授权、永久免费商用的开源内容管理系统。
- 基于流行的 ThinkPHP6.0 框架，方便快速上手，二次开发。
- 完善的模板标签，完善的模板布局，支持前台模板响应式与非响应分离式模板。
- 完善的后台系统，界面简洁，功能轻便，易上手。可支持在线升级与手动升级，保证网站的更新与安全。
- 完善的插件、模板在线安装与升级
- 项目长期维护，持续优化。

#### 演示站点
演示首页：[http://demo.hkcms.cn/](http://demo.hkcms.cn/)  
演示后台：[http://demo.hkcms.cn/demo.php](http://demo.hkcms.cn/demo.php)  

#### 软件架构
HkCms 基于 Thinkphp6.0 框架开发的一套开源内容管理系统，所需以下使用环境。
````  
PHP >= 7.2.5+
MySql >= 5.6
ThinkPHP V6.1.4
```` 

#### 安装教程

1.  手册地址：https://www.kancloud.cn/hkcms/hkcms_tp6/2252597
2.  帮助中心：http://www.hkcms.cn/help/

#### 交流群
官方 QQ 群：<a href="https://jq.qq.com/?_wv=1027&k=8YzV4elJ" target="_blank">808251031(HkCms 交流②群)</a>  
官方微信群：联系微信号 (cn-MrHua)，备注来源。  
官方网站：[http://www.hkcms.cn](http://www.hkcms.cn)  
社区问答：[http://www.hkcms.cn/help/](http://www.hkcms.cn/help/)

#### 版权信息
HkCms 遵循 Apache-2.0 开源协议发布，并提供免费使用。  
本项目包含的第三方源码和二进制文件之版权信息另行标注。  
版权所有 Copyright © 2022 by HkCms (https://www.hkcms.cn/)  
All rights reserved.